# GIT

## Yaksho vydaie pomylka git "client error, server certificate virification failed"

```git
$ git config --global http.sslverify false
```

## Zberezhennia lohinu i paroliu v GIT

```git
$ git config --global credential.helper store
```