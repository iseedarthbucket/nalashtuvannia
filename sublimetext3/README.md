# Sublime Text 3 Keys

 Komandy | Opys
:-------:|:----:
 Ctrl-`  | konsol
 Ctrl-Shift-p | zapusk package control
 F11 | povnoekranyi rezhim
 Ctrl-k-b | vidkryty/zakryty *sidebar*
 Ctrl-k Ctrl-*number* | zhortaie hilky
 Ctrl-k Ctrl-j | rozhortaie hilku

## Vydilennia

 Komanda | Opys
:-------:|:----:
 Ctrl-d | vydilyty slovona yakomu kursor / x2 vydilennia spivpadaiuchi
 Alt-F3 | znaity i vydilyty vse (vydilene slovo )
 Ctrl-l | vydilyty stroku
 Ctrl-Shift-m | vydilyty vmist duzhok
 Ctrl-Shift-*arrow right* | vydilyty slovo pravoruch vid kursora
 Ctrl-Shift-*arrow left* | vydilyty slovo livoruch vid kursora
 Ctrl-Shift-j | vydilyty stroky z identychnym vidstupom
 Shift-*right mouse clicl* | vertykalne vydilennia

## Poshuk

 Komanda | Opys
:-------:|:-----:
 Ctrl-p | poshuk konkretnoho failu v proekti
 Ctrl-f | poshuk u faili
 Enter | poshuk nastupnoho vkhodzhennia
 Alt-Enter | poshuk poperednie vkhodzhennia
 Shift-Enter | poshuk poperednie vkhodzhennia
 Esc | poshuk zakryty, vydilyty rezultat
 Alt-r | poshuk vkl/vykl reh. vyrazhennia
 Alt-c | poshuk vkl/vykl chutlyvosti k rehistru
 Alt-w | poshuk vkl/vykl poshuk slova tsilkom
 Ctrl-i | zhyvyi poshuk u faili
 Enter | zhyvyi poshuk zakryty, vydil. resultat
 Esc | zhyvyi poshuk zakryty
 Ctrl-Shift-f | poshuk i zamina po failakh v paptsi
 Ctrl-h | poshuk i zamin po failu
 Ctrl-Shift-h | zaminyty nastupne
 F3 | nastupne vkhodzhennia shukanoho
 Shift-F3 | poperednie vjhodzhennia shujanoho
 F4 | nastupne vkhodzhennia shukanoho (tilky v khodi poshuku po failam v paptsi )
 Shift-F4 | poperednie vkhodzhennia shukanoho (tilky v khodi poshuku po failakh v paptsi)

## Bloky KOdu

 Komanda | Opys
:-------:|:-----:
 Ctrl-Shift-[ | zhornuty potochnyi blok
 Ctrl-Shift-] | rozhornuty potochnyi blok
 Ctrl-/ | zakomentuvaty riadok
 Ctrl-Shift-/ | zakomentuvaty blok kodu (kursor v blotsi)

## Vkladky

 Komanda | Opys
:-------:|:-----:
 Ctrl-n | stvoryty vkladku
 Ctrl-w | zakryty vkladku
 Ctrl-s | zberehty
 Ctrl-Shift-s | zberehty yak
 Ctrl-Shift-1 ... 0 | peremistyty vkladku v obl z nomerom (pokazani 2 i bilshe oblastei)
 Alt-1 ... 0 | pokazaty vidpovidnu vkladku
 Ctrl-*PageUp* | poperednia vkladka
 Ctrl-*PageDown* | nastupna vkladka
 Ctrl-Tab | nastupna vkladka v stetsi
 Ctrl-Shift-Tab | poperednia vkladka v stetsi

## Tekst

 Komanda | Opys
:-------:|:-----:
 Ctrl-Shift-v | vstavyty z bufera z vidstupom
 Ctrl-Backspace | vydalyty slovo pered kursorom
 Ctrl-Del | vydalyty slovo pislia kursora
 Ctrl-Shift-Backspace | vydalyty riadok pered kursorom
 Ctrl-Shift-Del | vydalyty riadok pislia kursora
 Ctrl-Shift-d | produbliuvaty tekst (vydileno frahment tekstu)
 Ctrl-k-u | peretvoryty v propysni bukvy (vydileno frahment tekstu)
 Ctrl-k-l | peretvoryty v mali litery (vydileno frahment tekstu)
 F6 | perevirka orfohrafii vkl/vykl
 Tab | riaky dodaty riadku vidstup (kursor na pochatku riadka)
 Shift-Tab | prybraty riadku vidstup (kursor na pochatku riadka)
 Ctrl-] | dodaty riadky vidstup (kursor v bud-yakii pozytsii)
 Ctrl-[ | prybraty u riadku vidstup (kursor v bud-yakii pozytsii)
 Ctrl-Shift-*arrow up* | pidniaty riadok vhoru
 Ctrl-Shift-*arrow down* | opustyty riadok vnyz
 Ctrl-Shift-d | produbliuvaty riadok (nemaie vydilennia)
 Ctrl-Shift-Enter | vstavyty riadok vyshchie kursora
 Ctrl-Enter | vstavyty riadok nyzhchie kursora
 Ctrl-Shift-k | vydalyty riadok v yakii kursor
 Ctrl-j | obiednaty riadky

## Perekhody

 Komanda | Opys
:-------:|:-----:
 Ctrl-p | perekhid do failu v proekti (filtr)
 Ctrl-m | pereity do parnoi duzhky
 Ctrl-g | pereity do riadka
 Ctrl-F2 | postavyty na riadku zakladky
 F2 | k nastupnii zakladtsi
 Shift-F2 | k poperednoi zakladtsi
 Ctrl-Shift-F2 | prybraty vse zakladky

## Kursor

 Komanda | Opys
:-------:|:-----:
 Ctrl-Alt-*arrow up*/*arrow down* | dodaty kursor na storintsi vyshchie abo nyzhche
 Esc | prybraty multykursor
 Ctrl-*left mouse click* | dodaty multikursor

## Zovnishnii vyhliad

 Komandy | Opys
:-------:|:-----:
 F11 | povnyi ekran
 Shift-F11 | tilky oblast redahuvannia
 Alt-Shift-1 ... 4 | pokazaty odnu/kilka oblast red.
 Alt-Shift-5 | oblasti red-ia sitkoiu 2x2
 Alt-Shift-8 | oblasti red-ia v dvi linii
 Alt-Shift-9 | oblasti red-ia v try linii
 Ctrl-1 ... 4 | pereity do oblasti re-ia z nomerom
 Ctrl-0 | perekliuchytysia na bichnu panel

## Rizne

 Komanda | Opys
:-------:|:-----:
 Ctrl-*arrow up* | prokrutyty trokhy vhoru
 Ctrl-*arrow down* | prokrutyty trokhy vnyz

## Keybindings

 Komanda | Opys
:-------:|:-----:
 Alt-Shift-f | peremotuvannia
 Alt-Shift-t | vydilyty kintsevi probily
 Alt-Shift-d | peremykaie kintsevi probily
 Alt-Shift-1 | goto css deklaratsii
 Alt-Shift-n | rozhyrenyi novyi fail
 Ctrl-Alt-t | peremykannia paneli terminaliv
 Alt-` | terminal vidkryty
 F6 | chystyi css
 Enter | zavershennia komitu

## Plahin Emmet

 Komanda | Opys
:-------:|:-----:
 Tab | rozhornuty abreviaturu
 Ctrl-Alt-Enter | interaktyvna abreviatura (pyshemo abreviaturu i bachymo rezultat)
 Ctrl-Shift-; | prybraty teh, v yakomu kursor (tabuliatsiia zberezhetsia pravylnoiu
 Ctrl-, | vydilyty teh i ioho kontent (2x i dali - vydilyty i batkivskyi teh)
 Ctrl-Shift-0 | prybraty z vydilennia batkivskyi teh (zvorotnie "Ctrl ,")
 Ctrl-Alt-j | pereity do parnoho tehu
 Ctrl-Shift-g | obernuty vydilene v abreviaturu
 Ctrl-Alt-*arrow left*/*arrow right* | perekhid mizh tochkamy redahuvannia (tochka redahuvannia tse: porozhni tehy, porozhni atrybuty, porozhni riadky z vidstupamy)
 Ctrl-Shift-./, | vydilyty oblasti red-ia (nastupne/poperednie) (oblasti redahuvannia tse: tehy, atrybuty tsilkom, znachennia atrybutiv tsilkom, znachennia atrybutiv okremo
 Ctrl-Shift-/ | zakomentuvaty riadky ano blok (yakshcho kursor poza tehom, zakomentuite vmist batkivskoho teha)
 Ctrl-Shift-` | zrobyty teh parnym/neparnym
 Ctrl-u | onovyty/dodaty rozmiry kartynky
 Ctrl-Shift-y | pidrakhuvaty mat. vyraz
 Ctrl-Shift-r | synkhronizuvaty CSS-pravyla (kilka pravyl z vendornymy prefiksamy
 Ctrl-' | koduvaty/dekoduvaty kartynku v data: URL
 Ctrl-Shift-' | vydilyty vidkryty i zakryty teh (redahuvaty teh)
 Ctrl-*arrow up*/*arrow down* | zminyty chyslo na 1
 Alt-*arrow up*/*arrow down* | zminyty chyslo na 0.1
 Shift-Alt-*arrow up*/*arrow down* | zminyty chyslo na 10

## Plahin Trailing Spaces

 Komanda | Opys
:-------:|:-----:
 Ctrl-Shift-t | vydalyty kintsevi probily

## Plahin Zen Tabs

 Komanda | Opys
:-------:|:-----:
 Alt-Shift-r | zen tabs perezavantazhyty
 Alt-Tab | peremykannia vkladok

## Plahin Clipboard History

 Komanda | Opys
:-------:|:-----:
 Ctrl-Alt-v | pokazaty istoriiu
 Ctrl-Alt-d | ochystyty istoriiu
 Ctrl-Shift-v | vstavyty poperednii (staryi) zapys istorii
 Ctrl-Shift-Alt-v | vstavte nastupni (novishyi) zapys istorii

## Plahin Console Wrap

 Komanda | Opys
:-------:|:-----:
 Ctrl-Shift-q | console wrap
 Ctrl-Shift-Alt-q | console wrap
 Alt-Shift-f | peremotuvannia
 Alt-Shift-t | vydalyty trailing spaces
 Alt-Shift-d | peremykannia trailing spaces
 Alt-Shift-1 | goto css declaration
 Alt-Shift-n | rozshyrenyi novyi fail
 Ctrl-Alt-t | peremykannia termius panel
 Alt-` | vidkryty termius
 F6 | chystyi css
 Enter | zavershenia

## Plahin Termius

 Komanda | Opys
:-------:|:------:
 Alt-` | peremykannia terminal panel
 Ctrl-Alt-t | vidkryty vyhliad terminalu v potochnomu katalozi failiv
 Ctrl-w | zakryty terminal

## Plahin Sublime Linter

 Komanda | Opys
:-------:|:-----:
 Ctrl-k/l | lint tsei view
 Ctrl-k/a | pokazaty usi pomylky
 Ctrl-k/n | do novoi pomylky
 Ctrl-k/p | do poperednoi pomylky

## Plahin Pretty YAML

 Komanda | Opys
:-------:|:-----:
 Ctrl-Alt-y |

## Plahin jQuerry Docs

 Komanda | Opys
:-------:|:-----:
 Alt-j | jQuery dokumenty

## Plahin Rainbow Brackets

 Komanda | Opys
:-------:|:------:
 Ctrl-Alt-9 | Replace the brackets around the cursors with ()
 Ctrl-Alt-0 | Replace the brackets around the cursors with ()
 Ctrl-Alt-[ | Replace the brackets around the cursors with []
 Ctrl-Alt-] | Replace the brackets around the cursors with []
 Ctrl-Alt-Shift [ | Replace the brackets around the cursors with {}
 Ctrl-Alt-Shift ] | Replace the brackets around the cursors with {}
 Ctrl-Alt-r | Remove the brackets around the cursors
 Ctrl-Alt-. | Remove the brackets around the cursors and select the text within the brackets
 Ctrl-Alt-, | Select the brackets around the cursors and the text within the brackets