# Openbox desktop

## Vstanovlennia

osnovni pakunky
```zsh
$ sudo pacman -S lightmd lightdm-gtk-greeter openbox obconf pcmanfm
```
ale mozhna vstanovyty usi pakunky yaki potribno za bazhanniam
```zsh
$ sudo pacman -S tint2 xterm alacritty nitrogen geany unzip xarchiver gnome-background manumaker
```

## Aktyvuiemo LIGHTDM

```zsh
$ sudo systemctl enable lightdm
$ sudo reboot now
```

Robymo **reboot**, teper mozhemo vykorystovuvaty desktop

## Menu v OPENBOX

rekonfihuratsiia meniu
```zsh
$ mmaker openbox -f -t terminal
```