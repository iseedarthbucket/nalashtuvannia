# ViM Keys

 Komanda | Opys
:-------:|:-----:
 ESC     | indikator znykaie, i vy povertai'tes do komandnoho rezhymu

## Peremishchiennia

 Komanda | Opys
:-------:|:-----:
 h       | vlivo
 j       | vnyz
 k       | dohory
 l       | vpravo

## Vydalyty symvoly

 Komanda | Opys
:-------:|:-----:
 x       | shchob vydalyty symvol, navedit kursor na noho ta vvedit

## Skasuvaty ta Povtoryty

 Komanda | Opys
:-------:|:-----:
 u       | komanda skasovuie ostannie redahuvannia
 CTRL r  | povtoryty

## Vykhid

 Komanda | Opys
:-------:|:-----:
 z z     | komanda zapysuie fail s vykhid

## Vidmina zmin

 Komanda | Opys
:-------:|:-----:
 :q!     | kynte i vykynte rechi