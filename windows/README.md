# Vstanovlennia Windows

Pry vstanovlenni **Windows 10** na poxhatku nalashtuvan nazhymaiemo ```CTRL+SHIFT+F3```, **windows** perezavantazhuietsia u audit mod i zavantazhuie admin akaunt, pislia zavantazhennia ziavytsia **System Preparation Tool**, nazhymaiemo **Cancel**. Vidkryvaiemo *Notepad* i kopiiuiemo.
```xml
<?xml version="1.0" encoding="utf-8"
<unattend xmlns="urn:schemas-microsoft-com:wnattend">
<settings pass="oobeSystem">
<component name="Microsoft-Windows-Shell-Setup" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instante">
<FolderLocations>
<ProfilesDirectory>D:\Users</ProfilesDirectory>
</FolderLocations>
</component>
</settings>
</unattend>
```
zberihaiemo fail pid nazvoiu "*relocate.xml*" na "*D:*"
zapuskaiemo **Comand Prompt**
zupyniaiemo **WMP Network Sharing Service**
```cmd
net stop wmpnetworksvc
```
teper pryminiaiemo zminy
```cmd
%windir%\system32\sysprep\sysprep.exe /oobe /reboot /unattend:d:\relocate.xml
```
Yaksho vse normalno **windows** perezavantazhuiemo i pochnetsia nalashtuvannia **windows**. 
Pislia vstanovlennia papka **users** povynna buty na dysku *D:*

## Pislia vstanovlennia Windows 10

* pereviriaiemo naiavnist onovlen
* yakshcho ie vstanovliuiemo onovlennia
* u Windows 10 mozhna vykorystovuvaty **Windows Defender** yak antyvirus, abo vstanovyty antyvirus po bazhabbiu
* onovliuiemo **Microsoft Edge**
* vstanovliuiemo draivera